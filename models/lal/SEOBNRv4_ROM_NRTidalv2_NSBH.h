// AUTHOR :  Michael Puerrer
//           Michael.Puerrer@ligo.org
//
// DATE: 2023
//
// PURPOSE: Interface with LAL SEOBNRv4_ROM_NRTidalv2_NSBH model
//          https://www.lsc-group.phys.uwm.edu/daswg/projects/lalsuite.html
//

#ifndef SEOBNRv4_ROM_NRTIDALV2_NSBH_LAL_HPP
#define SEOBNRv4_ROM_NRTIDALV2_NSBH_LAL_HPP

extern "C"{
#ifdef __cplusplus
  // Workaround to allow C++ programs to use stdint.h macros specified in the C99 standard that aren't in the C++ standard.
  #define __STDC_CONSTANT_MACROS
  #ifdef _STDINT_H
    #undef _STDINT_H
  #endif
  #include <stdint.h>
#endif
}

//#include <iostream>
#include <stdio.h>
#include <math.h>
#include <lal/LALConstants.h>
#include <lal/LALSimIMR.h>
//#include <stdint.h>
#include <lal/Sequence.h>
#include <lal/FrequencySeries.h>


// This routine is interfaced with greedy routine -- returns gsl data type //
void SEOBNRv4_ROM_NRTidalv2_NSBH_FullWaveform(
  gsl_vector_complex * &wv, 
  const gsl_vector *fnodes,
  const double *params,
  const std::string model_tag)
{
  // params = {m1,m2,chi1,chi2,lambda2}
  // parameter list such that (m1(param),m2(param),chi1,chi2,lambda2) 
  // is a unique point in parameter space

  // --- deduce the model_part tag --- //
  std::string model_part =
    lal_help::model_tag2mode_part(model_tag, 5, params);


  // Note: We expect masses in units of solar mass 
  // -> use conversion factor 1 in cfg-file!
  double m1SI = params[0] * LAL_MSUN_SI;
  double m2SI = params[1] * LAL_MSUN_SI;
  double chi1 = params[2];
  double chi2 = params[3];
  double lambda2 = params[4];
  double lambda1 = 0.0; // This is the BH
  double distance = 100*1e6*LAL_PC_SI;
  double inclination = 0;
  double fRef = 0;
  double phiRef = 0;

  // Define LALpars and put tidal deformabilities into this dict
  LALDict *LALpars=XLALCreateDict();
  XLALSimInspiralWaveformParamsInsertTidalLambda1(LALpars, lambda1);
  XLALSimInspiralWaveformParamsInsertTidalLambda2(LALpars, lambda2);
  // Now compute quadrupole parameters from the tidal deformabilities
  int ret = XLALSimInspiralSetQuadMonParamsFromLambdas(LALpars);
  if (ret != XLAL_SUCCESS) {
    fprintf(stderr, "Failed to set QuadMon from Lambdas for SEOBNRv4_ROM_NRTidalv2_NSBH.\n");
    exit(-1);
  } 

  int n = fnodes->size;
  // Copy frequency data into sequence
  const REAL8Sequence *freqs = XLALCreateREAL8Sequence(n);
  for (int i=0; i<n; i++)
    freqs->data[i] = gsl_vector_get(fnodes, i);

  struct tagCOMPLEX16FrequencySeries *hptilde = NULL;
  struct tagCOMPLEX16FrequencySeries *hctilde = NULL;

  /** Compute waveform in LAL format at specified frequencies */

  ret = XLALSimIMRSEOBNRv4ROMNRTidalFrequencySequence(
    &hptilde,      /**< Output: Frequency-domain waveform h+ */
    &hctilde,      /**< Output: Frequency-domain waveform hx */
    freqs,         /**< Frequency points at which to evaluate the waveform (Hz) */
    phiRef,        /**< Phase at reference time */
    fRef,          /**< Reference frequency (Hz); 0 defaults to fLow */
    distance,      /**< Distance of source (m) */
    inclination,   /**< Inclination of source (rad) */
    m1SI,          /**< Mass of companion 1 (kg) */
    m2SI,          /**< Mass of companion 2 (kg) */
    chi1,          /**< Dimensionless aligned component spin 1 */
    chi2,          /**< Dimensionless aligned component spin 2 */
    lambda1,       /**< Dimensionless tidal deformability 1 */
    lambda2,       /**< Dimensionless tidal deformability 2 */
    LALpars,       /**< linked list containing extra parameters */
    NRTidalv2NSBH_V /**< Version of NRTides */
  );

  if (ret != XLAL_SUCCESS) {
    fprintf(stderr, "Error calling XLALSimIMRSEOBNRv4ROMNRTidalFrequencySequence().\n");
    exit(-1);
  }

  lal_help::lal_waveform_part(wv, model_part, hptilde, hctilde, n);

  XLALDestroyREAL8Sequence((REAL8Sequence *)freqs);
  XLALDestroyCOMPLEX16FrequencySeries(hptilde);
  XLALDestroyCOMPLEX16FrequencySeries(hctilde);
}

#endif // SEOBNRv4_ROM_NRTIDALV2_NSBH_LAL_HPP


// AUTHOR :  Michael Puerrer
//           Michael.Puerrer@ligo.org
//
// DATE: 2020
//
// PURPOSE: Interface with LAL SEOBNRv4HM_ROM
//          https://www.lsc-group.phys.uwm.edu/daswg/projects/lalsuite.html
//

#ifndef ROM_SEOBNRv4HMLAL_HPP
#define ROM_SEOBNRv4HMLAL_HPP

extern "C"{
#ifdef __cplusplus
  // Workaround to allow C++ programs to use stdint.h macros specified in the C99 standard that aren't in the C++ standard.
  #define __STDC_CONSTANT_MACROS
  #ifdef _STDINT_H
    #undef _STDINT_H
  #endif
  #include <stdint.h>
#endif
}

//#include <iostream>
#include <stdio.h>
#include <math.h>
#include <lal/LALConstants.h>
#include <lal/LALSimIMR.h>
#include <lal/Units.h>
#include <lal/SphericalHarmonics.h>
#include <lal/LALSimSphHarmMode.h>
//#include <stdint.h>
#include <lal/Sequence.h>
#include <lal/FrequencySeries.h>


// This routine is interfaced with greedy routine -- returns gsl data type //
void ROM_SEOBNRv4HM_FullWaveform(
  gsl_vector_complex * &wv, 
  const gsl_vector *fnodes,
  const double *params,
  const std::string model_tag)
{
  // params = {m1, m2, chi1, chi2, inclination, phiRef}
  // parameter list such that (m1(param),m2(param),chi1,chi2,inclination,phiRef) is a unique point in parameter space

  // --- deduce the model_part tag --- //
  std::string model_part =
    lal_help::model_tag2mode_part(model_tag, 6, params);


  // Note: We expect masses in units of solar mass 
  // -> use conversion factor 1 in cfg-file!
  double m1SI = params[0] * LAL_MSUN_SI;
  double m2SI = params[1] * LAL_MSUN_SI;
  double chi1 = params[2];
  double chi2 = params[3];
  double distance = 100*1e6*LAL_PC_SI;
  double inclination = params[4];
  double fRef = 0;
  double phiRef = params[5];


  int n = fnodes->size;
  // Copy frequency data into sequence
  const REAL8Sequence *freqs = XLALCreateREAL8Sequence(n);
  for (int i=0; i<n; i++)
    freqs->data[i] = gsl_vector_get(fnodes, i);

  struct tagCOMPLEX16FrequencySeries *hptilde = NULL;
  struct tagCOMPLEX16FrequencySeries *hctilde = NULL;

  LALDict *LALparams = NULL;

  /** Compute waveform in LAL format at specified frequencies */
  int nModes = 5; // All HMs included in model: 
  // {2, 2}, {3, 3}, {2, 1}, {4, 4}, {5, 5}
  // The ROM modes are hybridized with PN on the fly.

  int ret = XLALSimIMRSEOBNRv4HMROMFrequencySequence(
    &hptilde,     /**< Output: Frequency-domain waveform h+ */
    &hctilde,     /**< Output: Frequency-domain waveform hx */
    freqs,        /**< Frequency points at which to evaluate the waveform (Hz) */
    phiRef,       /**< Phase at reference time */
    fRef,         /**< Reference frequency (Hz); 0 defaults to fLow */
    distance,     /**< Distance of source (m) */
    inclination,  /**< Inclination of source (rad) */
    m1SI,         /**< Mass of companion 1 (kg) */
    m2SI,         /**< Mass of companion 2 (kg) */
    chi1,         /**< Dimensionless aligned component spin 1 */
    chi2,         /**< Dimensionless aligned component spin 2 */
    -1,           /**< Truncate interpolants at SVD mode nk_max; don't truncate if nk_max == -1 */
    nModes,       /**< Number of modes to use. This should be 1 for SEOBNRv4_ROM and 5 for SEOBNRv4HM_ROM */
    LALparams     /**<< Dictionary of additional wf parameters, here is used to pass ModeArray */
  );

  if (ret != XLAL_SUCCESS) {
    fprintf(stderr, "Error calling XLALSimIMRSEOBNRv4HMROMFrequencySequence().\n");
    exit(-1);
  }

  lal_help::lal_waveform_part(wv,model_part,hptilde,hctilde,n);

  XLALDestroyREAL8Sequence((REAL8Sequence *)freqs);
  XLALDestroyCOMPLEX16FrequencySeries(hptilde);
  XLALDestroyCOMPLEX16FrequencySeries(hctilde);
}


// This routine is interfaced with greedy routine -- returns gsl data type //
void ROM_SEOBNRv4HM_GetMode(
  gsl_vector_complex * &wv,
  const gsl_vector *fnodes,
  const double *params,
  const std::string model_tag)
{
  // params = {m1, m2, chi1, chi2, ell, m}
  // parameter list such that (m1(param), m2(param), chi1, chi2, ell, m) is a unique point in parameter space

  // --- set the model_part tag --- //
  // mode or modesqr
  std::string model_part = lal_help::get_waveform_part_tag(model_tag);

  // Note: We expect masses in units of solar mass
  // -> use conversion factor 1 in cfg-file!
  double m1SI = params[0] * LAL_MSUN_SI;
  double m2SI = params[1] * LAL_MSUN_SI;
  double chi1 = params[2];
  double chi2 = params[3];
  int ell = round(params[4]);
  int m = abs(round(params[5]));

  double fRef = 0.0;
  double phiRef = 0.0;
  double distance = 100*1e6*LAL_PC_SI;
  double inclination = 0.0;

  int n = fnodes->size;
  // Copy frequency data into sequence
  const REAL8Sequence *freqs = XLALCreateREAL8Sequence(n);
  for (int i=0; i<n; i++)
    freqs->data[i] = gsl_vector_get(fnodes, i);

  SphHarmFrequencySeries *hlm = NULL;
  LALDict *LALparams = NULL;

  /** Compute waveform in LAL format at specified frequencies */
  int nModes = 5; // All HMs included in model:
  const unsigned int modes[5][2] = {{2, 2}, {3, 3}, {2, 1}, {4, 4}, {5, 5}};

  bool valid_mode = false;
  for (int i=0; i < 5; i++)
    if ((ell == modes[i][0]) && (m == modes[i][1]))
      valid_mode = true;

  if (valid_mode == false) {
    fprintf(stderr, "ROM_SEOBNRv4HM_GetMode: (%d, %d)-mode is not supported!\n", ell, m);
    exit(-1);
  }


  int ret = XLALSimIMRSEOBNRv4HMROMFrequencySequence_Modes(
      &hlm,            /**< Output: Frequency-domain hlm */
      freqs,           /**< Frequency points at which to evaluate the waveform (Hz) */
      phiRef,          /**< Phase at reference time */
      fRef,            /**< Reference frequency (Hz); 0 defaults to fLow */
      distance,        /**< Distance of source (m) */
      inclination,     /**< Inclination of source (rad) */
      m1SI,            /**< Mass of companion 1 (kg) */
      m2SI,            /**< Mass of companion 2 (kg) */
      chi1,            /**< Dimensionless aligned component spin 1 */
      chi2,            /**< Dimensionless aligned component spin 2 */
      -1,              /**< Truncate interpolants at SVD mode nk_max; don't truncate if nk_max == -1 */
      nModes,          /**< Number of modes to use. This should be 1 for SEOBNRv4_ROM and 5 for SEOBNRv4HM_ROM */
      LALparams        /**<< Dictionary of additional wf parameters, here is used to pass ModeArray */
    );

  if (ret != XLAL_SUCCESS) {
    fprintf(stderr, "Error calling XLALSimIMRSEOBNRv4HMROMFrequencySequence_Modes().\n");
    exit(-1);
  }


  // Grab selected mode and set waveform data array
  // hlm_selected points inside hlm, i.e. not a copy.
  COMPLEX16FrequencySeries *hlm_selected = XLALSphHarmFrequencySeriesGetMode(hlm, ell, -m);
  lal_help::lal_waveform_part(wv, model_part, hlm_selected, hlm_selected, n);

  XLALDestroyREAL8Sequence((REAL8Sequence *)freqs);
  XLALDestroySphHarmFrequencySeries(hlm);
}



// This routine is interfaced with greedy routine -- returns gsl data type //
void ROM_SEOBNRv4HM_GetMode_PlusCross(
  gsl_vector_complex * &wv,
  const gsl_vector *fnodes,
  const double *params,
  const std::string model_tag)
{
  // params = {m1, m2, chi1, chi2, inclination, phiRef, ell, m}
  // parameter list such that (m1(param), m2(param), chi1, chi2, inclination, phiRef, ell, m) is a unique point in parameter space

  // --- set the model_part tag --- //
  // SEOBNRv4HM_ROM_mode_*
  std::string model_part =
    lal_help::model_tag2mode_part(model_tag, 8, params);

  // Note: We expect masses in units of solar mass 
  // -> use conversion factor 1 in cfg-file!
  double m1SI = params[0] * LAL_MSUN_SI;
  double m2SI = params[1] * LAL_MSUN_SI;
  double chi1 = params[2];
  double chi2 = params[3];
  double distance = 100*1e6*LAL_PC_SI;
  double inclination = params[4];
  double fRef = 0;
  double phiRef = params[5];
  int ell = round(params[6]);
  int m = abs(round(params[7]));


  int n = fnodes->size;
  // Copy frequency data into sequence
  const REAL8Sequence *freqs = XLALCreateREAL8Sequence(n);
  for (int i=0; i<n; i++)
    freqs->data[i] = gsl_vector_get(fnodes, i);

  SphHarmFrequencySeries *hlm = NULL;
  LALDict *LALparams = NULL;

  /** Compute waveform in LAL format at specified frequencies */
  int nModes = 5; // All HMs included in model:
  const unsigned int modes[5][2] = {{2, 2}, {3, 3}, {2, 1}, {4, 4}, {5, 5}};

  bool valid_mode = false;
  for (int i=0; i < 5; i++)
    if ((ell == modes[i][0]) && (m == modes[i][1]))
      valid_mode = true;

  if (valid_mode == false) {
    fprintf(stderr, "ROM_SEOBNRv4HM_GetMode: (%d, %d)-mode is not supported!\n", ell, m);
    exit(-1);
  }


  int ret = XLALSimIMRSEOBNRv4HMROMFrequencySequence_Modes(
      &hlm,            /**< Output: Frequency-domain hlm */
      freqs,           /**< Frequency points at which to evaluate the waveform (Hz) */
      phiRef,          /**< Phase at reference time */
      fRef,            /**< Reference frequency (Hz); 0 defaults to fLow */
      distance,        /**< Distance of source (m) */
      inclination,     /**< Inclination of source (rad) */
      m1SI,            /**< Mass of companion 1 (kg) */
      m2SI,            /**< Mass of companion 2 (kg) */
      chi1,            /**< Dimensionless aligned component spin 1 */
      chi2,            /**< Dimensionless aligned component spin 2 */
      -1,              /**< Truncate interpolants at SVD mode nk_max; don't truncate if nk_max == -1 */
      nModes,          /**< Number of modes to use. This should be 1 for SEOBNRv4_ROM and 5 for SEOBNRv4HM_ROM */
      LALparams        /**<< Dictionary of additional wf parameters, here is used to pass ModeArray */
    );

  if (ret != XLAL_SUCCESS) {
    fprintf(stderr, "Error calling XLALSimIMRSEOBNRv4HMROMFrequencySequence_Modes().\n");
    exit(-1);
  }


  // Compute the "h+" and "hx" parts for the selected (ell, m) mode
  //
  // Note: It would be easier to call XLALSimIMRSEOBNRv4HMROMFrequencySequence
  // and select a single mode via a modearray.
  // First allocate frequency series for hp, hc

  /* GPS time for output frequency series and modes */
  COMPLEX16FrequencySeries *hlm_selected = XLALSphHarmFrequencySeriesGetMode(hlm, ell, -m);
  LIGOTimeGPS tGPS = hlm_selected->epoch;
  size_t npts = hlm_selected->data->length;
  double deltaF = 0.0;
  COMPLEX16FrequencySeries *hp_mode = XLALCreateCOMPLEX16FrequencySeries("hptilde: FD waveform", &tGPS, 0.0, deltaF, &lalStrainUnit, npts);
  COMPLEX16FrequencySeries *hc_mode = XLALCreateCOMPLEX16FrequencySeries("hctilde: FD waveform", &tGPS, 0.0, deltaF, &lalStrainUnit, npts);
  memset(hp_mode->data->data, 0, npts * sizeof(COMPLEX16));
  memset(hc_mode->data->data, 0, npts * sizeof(COMPLEX16));
  XLALUnitMultiply(&hp_mode->sampleUnits, &hp_mode->sampleUnits, &lalSecondUnit);
  XLALUnitMultiply(&hc_mode->sampleUnits, &hc_mode->sampleUnits, &lalSecondUnit);

  // Grab selected mode and set waveform data array
  // hlm_selected points inside hlm, i.e. not a copy.
  XLALSimAddModeFD(hp_mode, hc_mode, hlm_selected, inclination, LAL_PI/2. - phiRef, ell, -m, 1);

  lal_help::lal_waveform_part(wv, model_part, hp_mode, hc_mode, n);


  XLALDestroyREAL8Sequence((REAL8Sequence *)freqs);
  XLALDestroySphHarmFrequencySeries(hlm);
  XLALDestroyCOMPLEX16FrequencySeries(hp_mode);
  XLALDestroyCOMPLEX16FrequencySeries(hc_mode);
}




#endif


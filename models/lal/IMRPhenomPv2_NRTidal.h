// AUTHOR :  Michael Puerrer
//           Michael.Puerrer@ligo.org
//
// DATE: 2019
//
// PURPOSE: Interface with LAL IMRPhenomPv2_NRTidal
//          https://www.lsc-group.phys.uwm.edu/daswg/projects/lalsuite.html
//
// Based on phenomp.h

#ifndef IMRPhenomPv2_NRTidal_LAL_HPP
#define IMRPhenomPv2_NRTidal_LAL_HPP

extern "C"{
#ifdef __cplusplus
  // Workaround to allow C++ programs to use stdint.h macros specified in the C99 standard that aren't in the C++ standard.
  #define __STDC_CONSTANT_MACROS
  #ifdef _STDINT_H
    #undef _STDINT_H
  #endif
  #include <stdint.h>
#endif
}

//#include <iostream>
#include <stdio.h>
#include <math.h>

#include <lal/LALConstants.h>
#include <lal/LALSimIMR.h>
//#include <stdint.h>
#include <lal/Sequence.h>
#include <lal/FrequencySeries.h>

#include "lal_helpers.hpp"

/*----------------------------------------------------------------------------
	Calls a modified version of XLALSimIMRPhenomP that allows the waveform
	to be computed for an array of arbitrary ordered frequencies.
 -----------------------------------------------------------------------------*/


void IMRPhenomPv2_NRTidal_Waveform(gsl_vector_complex *wv,
                    const gsl_vector *fnodes,
                    const double *params,
                    const std::string model_tag)
{


  // --- deduce the model_part tag --- //
  std::string model_part =
    lal_help::model_tag2mode_part(model_tag,9,params);


  COMPLEX16FrequencySeries *hptilde = NULL;
  COMPLEX16FrequencySeries *hctilde = NULL;

  int n = fnodes->size;
  const REAL8 m1_Msun = params[0];
  const REAL8 m2_Msun = params[1];
  const REAL8 m1_SI = m1_Msun * LAL_MSUN_SI;
  const REAL8 m2_SI = m2_Msun * LAL_MSUN_SI;
  const REAL8 chi1L = params[2];
  const REAL8 chi2L = params[3];
  const REAL8 chip = params[4];
  const REAL8 thetaJ = params[5];
  const REAL8 distance = 1;
  const REAL8 phic = 0;
  const REAL8 f_ref = 40;
  const REAL8 alpha0 = params[6];
  const REAL8 lambda1 = params[7];
  const REAL8 lambda2 = params[8];
  IMRPhenomP_version_type version_flag = IMRPhenomPv2NRTidal_V;
  NRTidal_version_type NRTidal_version = NRTidalv2_V;
  // See tags defined in LALSimIMR.h

  LALDict *extraParams = XLALCreateDict();
  XLALSimInspiralWaveformParamsInsertTidalLambda1(extraParams, lambda1);
  XLALSimInspiralWaveformParamsInsertTidalLambda2(extraParams, lambda2);

  // use XLALSimIMRPhenomPFrequencySequence with frequency sequence //
  const REAL8Sequence *freqs = XLALCreateREAL8Sequence(n);
  for (int i=0; i<n; i++) {
    freqs->data[i] = gsl_vector_get(fnodes, i);
  }

  int ret = XLALSimIMRPhenomPFrequencySequence(
    &hptilde,   //< Output: Frequency-domain waveform h+ //
    &hctilde,   //< Output: Frequency-domain waveform hx //
    freqs,      //< Frequency points at which to evaluate the waveform (Hz) //
    chi1L,      //< aligned spin of companion 1 //
    chi2L,      //< aligned spin of companion 2 //
    chip,       //< Effective spin in the orbital plane //
    thetaJ,     //< Angle between J0 and line of sight (z-direction) //
    m1_SI,      //< Mass of companion 1 //
    m2_SI,      //< Mass of companion 2 //
    distance,   //< Distance of source (m) // 
    alpha0,     //< Initial value of alpha angle (azimuthal precession angle) //
    phic,       //< Orbital phase at the peak of the underlying non precessing model (rad) //
    f_ref,      //< Reference frequency //
    version_flag,
    NRTidal_version,
    extraParams);

  if (ret != XLAL_SUCCESS) {
    fprintf(stderr, "Error calling XLALSimIMRPhenomPFrequencySequence().\n");
    exit(-1);
  }

  lal_help::lal_waveform_part(wv,model_part,hptilde,hctilde,n);

  XLALDestroyREAL8Sequence((REAL8Sequence *)freqs);
  XLALDestroyCOMPLEX16FrequencySeries(hptilde);
  XLALDestroyCOMPLEX16FrequencySeries(hctilde);
}

#endif // IMRPhenomPv2_NRTidal_LAL_HPP

